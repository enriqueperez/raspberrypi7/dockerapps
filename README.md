# Docker Apps in raspberry pi

Deploy a set of applications in a raspberry pi with `ansible`, `docker` and `docker-compose`. Check [list](/roles/apps/files/) of applications


![Raspberry pi 4 Model B, now with 8gb!](/assets/images/raspb4.jpg)


### Prerequisites

* Ansible `>= 2.7`
* Raspberry pi running with Rasbperry OS 32bits
* Sudo SSH access to the raspberry.
>**Note**: It's always recommended to use the private key authentication method to avoid using flag '-k' in ansible which requests the ssh password. Just generate a private key, then add it to the user account. After you will be able to connect without using password.
> `ssh-keygen`, `ssh-key-add {username}@{raspberry}`
***

## STEPS


#### 1. Clone the respository: [repolink](https://gitlab.com/enriqueperez/raspberrypi7/dockerapps.git)

```bash
git clone https://gitlab.com/enriqueperez/raspberrypi7/dockerapps.git
cd dockerapps/playbooks
```

#### 2. Update the inventory file `inventory`

```bash
vi ../inventory
raspberry ansible_ssh_host={replace_me} ansible_ssh_user={replace_me}
```

- `ansible_ssh_host`-> The raspberry hostname / ip address.
- `ansible_ssh_user`-> The ssh user. The user needs 'sudo' permissions.

#### 4. Deploy the configuration and APP's config

```bash
ansible-playbook deploy.yml
```

>**Note:** In case errors loading the local ansible configuration file, use the environment
config var
> `export ANSIBLE_CONFIG=$(pwd)/ansible.cfg`

This will install common tools, docker and docker compose. After it will deploy application configurations to the raspberry. Additionally performs a configuration backup of the configs

- `/opt/applications`
- `/opt/backup`

Once the configuration is deployed you can perform the following available operations for each application in next section:
* `start`
* `stop`
* `restart`
* `destroy` *(stop and remove data)*
***

##### `Start` an application

Run the following playbook to build and start the application

```bash
ansible-playbook start.yml -e "app={replace_me}"
```

- `app` -> Replace the exact name of the application folder you want to start.

>**Note:** You can find all the application folders under `./roles/apps/files/`

##### `Stop` an application

Run the following playbook to stop the application

```bash
ansible-playbook stop.yml -e "app={replace_me}"
```

##### `Restart` an application

Run the following playbook to restart the application

```bash
ansible-playbook restart.yml -e "app={replace_me}"
```

##### `Destroy` an application

Run the following playbook to stop and destroy the application

```bash
ansible-playbook destroy.yml -e "app={replace_me}"
```

***

##### Default Accesses

Most of the applications are set with the following credentials:

>Username: **thomasturbado** /
>Password: **Mooti2re**


##### Custom configurations

There are global configurations for the app deployment under:
`group_vars/main.yml`

However updating them will affect on the configurations of the apps. So these
may needed to be updated too with new paths.

The apps have a generic password, which can be updated accordingly in the configurations
under `docker-compose.yml` files or at `.env` files vars. Some of the apps may have no
vars on the other hand.

##### Development updates

For any change and updates, you will need to re-deploy the configuration into pi using `deploy` playbook and then restart the application. You may want to use tag `configs` which will be update only the apps configuration and perform a backup.

```bash
ansible-playbook deploy.yml -t configs
```
