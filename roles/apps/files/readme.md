#### List of applications

Place under this path each configured docker application to be run with
`docker-compose`


##### Requisites

* Each application must be in `docker-compose` format
* You can create a `.env` file within the folder to store all vars
* Map the vars from .env into docker-compose file. Example:
>* mysql_root_password=Mooti2re
>* MYSQL_ROOT_PASSWORD: ${mysql_root_password}

##### Structure of the folder:

>* appname
>>* docker-compose.yml
>>* .env


##### Applications

* [portainer](https://www.portainer.io/)
* [portainer-ce](https://www.portainer.io/)
* [pihole](https://pi-hole.net/)
* [samba](https://hub.docker.com/r/dperson/samba)
* [grafana](https://grafana.com/)
* [filebrowser](https://github.com/filebrowser/filebrowser)
* [glances](https://nicolargo.github.io/glances/)
* [owncloud](https://owncloud.org/)
* [wordpress](https://wordpress.com/)
* [duplicati](https://www.duplicati.com/)
* [netdata](https://github.com/netdata/netdata)
* [plex](https://hub.docker.com/r/linuxserver/plex)
* [yacht](https://yacht.sh/)
* [transmission](https://hub.docker.com/r/linuxserver/transmission)
* [librenms](https://www.librenms.org/)
* [netdata](https://github.com/netdata/netdata)

*The apps listed belong to their respective owners*
