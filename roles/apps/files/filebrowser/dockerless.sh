#!/bin/bash

touch ${pwd}/filebrowser.json
touch ${pwd}/database.db

cat << EOF > ./filebrowser.json
{
  "port": 80,
  "baseURL": "",
  "address": "",
  "log": "stdout",
  "database": "/database.db",
  "root": "/srv"
}
EOF

docker run \
   -v /:/srv \
   -v ${pwd}/database.db:/database.db \
   -v ${pwd}/filebrowser.json:/.filebrowser.json \
   -p 8002:80 \
   n0cloud/filebrowser-multiarch:latest
