#!/bin/bash

# Pull latest image for the service [librenms, mariadb]
docker-compose pull

# bring up service forcing recreate and build and daemonize
docker-compose up --force-recreate --build -d
