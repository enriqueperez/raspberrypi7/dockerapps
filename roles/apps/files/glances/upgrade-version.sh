#!/bin/bash

# Pull latest image for the service
docker-compose pull glances

# bring up service forcing recreate and build and daemonize
docker-compose up --force-recreate --build -d
