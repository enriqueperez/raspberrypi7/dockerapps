#!/bin/bash

# Pull latest image for the service
docker-compose pull prometheus
docker-compose pull ca-advisor
docker-compose pull grafana
docker-compose pull node-exporter

# bring up service forcing recreate and build and daemonize
docker-compose up --force-recreate --build -d
